#!/bin/sh
set -e

# This entrypoint script just sets the correct permissions on the
# shared volume and executes CMD with the less-privileged user "www-data"
# See https://github.com/moby/moby/issues/22258 for more information
# on the issue with permissions on shared volume.

# get uid of the shared volume which is the uid from the host directory
UID=${UID:-$(stat -c '%u' ${BAIKAL_DATA})}

# if UID is already remapped or we are root (like on macOS) do nothing
# if not set uid of "www-data" to the host's uid of the shared volume
if [ "$(id -u www-data)" -ne "${UID}" -a "${UID}" -ge "0" ]; then
  mkdir -p ${BAIKAL_DATA}/db
  # we will install usermod here to change the uid of the user "www-data"
  # see https://github.com/chrootLogin/docker-nextcloud/issues/3
  echo "Modding uid of user www-data to ${UID}"
  apk --no-cache add shadow
  usermod -u ${UID} www-data || true
  chown -R www-data:www-data ${BAIKAL_DATA}
  chown -R www-data:www-data ${BAIKAL_CONFIG}
  apk del -rf --purge shadow
fi

# print uid/gid of "www-data" for verification
su-exec www-data:www-data id
# run CMD from Dockerfile or from a `docker run` as user "www-data" (non-root)
exec su-exec www-data:www-data "$@"
