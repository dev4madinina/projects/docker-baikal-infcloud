default: build

image = pim-manager
container = $(image)-server
tag = 1.0.0
timezone = Europe/Paris

build:
	sudo docker build \
			--build-arg TIMEZONE=$(timezone) \
			--tag "$(image):$(tag)" \
			--tag "$(image):latest" \
			$(args) .

export:
	rm "$(image).$(tag).tgz" || true
	sudo docker image save --output "$(image).$(tag).tgz" "$(image):$(tag)"
	sudo chown ${USER}:${USER} "$(image).$(tag).tgz"

run:
	sudo docker run \
	    --name $(container) \
	    --restart unless-stopped \
	    --detach \
			--publish 8800:8800 \
			--volume "$$(pwd)/baikal/Specific:/var/www/baikal/Specific" \
			--volume "$$(pwd)/baikal/config:/var/www/baikal/config" \
			$(args) $(image)

stop:
	sudo docker stop $(container) && sudo docker rm $(container)

upgrade:
	make stop
	make build
	make run
