# docker-baikal-infcloud

<br>

---
_Forked from [bjuretko/docker-baikal-infcloud (GitHub)](https://github.com/bjuretko/docker-baikal-infcloud)_

> See branch [initial](https://gitlab.com/dev4madinina/projects/docker-baikal-infcloud/-/tree/initial?ref_type=heads) or directly [GitHub](https://github.com/bjuretko/docker-baikal-infcloud) to get the initial project from the author
> Diff :
>   - Synology not tested
>   - No MySql but Sqlite
>   - Doker image name &rarr; `pim-manager` _(instead of `docker-baikal-infcloud`)_
>   - Latest versions of all

---

<br>

- Dockerized
- [lighttpd](https://www.lighttpd.net/)
- php7 + sqlite3 and mysql support
- [baikal](https://github.com/sabre-io/Baikal) 0.9.3
- [infcloud](https://www.inf-it.com/open-source/clients/infcloud/) 0.1.13
- [Alpine Linux](https://mirrordocker.com/_/alpine) 3.18.3


![Infcloud webinterface](doc/infcloud.png)

# Why?

- Alternatives like owncloud/nextcloud or other colaboration suites are too much for this simple usecase
- Using baikal for several years without any bigger issues makes it pretty robust for family or internal small company/team management.
- You do not need email or other shared services
- Widely supported clients, unfortunetly not natively with Android.
- create backups of calendar and contact data
- Syncs well with Windows, MacOS, Android and iOS


# Build and Run

## Build docker image

To build the Docker image just call

```bash
make
```

You can provide additional arguments to the docker command like

```bash
make args=--no-cache
```

to export the image as a tarball:

```bash
make export
```

See Makefile for more information.

## Run

### With sqlite

First **build** the image. Then call

```bash
make run
```

And point your Browser http://localhost:8800/baikal/html/admin/ to configure baikal.
> [useful color-converter](https://www.useotools.com/fr/color-converter)

To start the container detached (background) exec

```bash
make run args="--hostname pim-manager"
```



## Timezone

You can change the timezone of the container on build-time via the
`docker build --build-arg TIMEZONE=Europe/Paris ...` command-line option
or set the timezone in the `Makefile`

# Default config

- HTTP port: 8800
- no mail support
- Cal/CardDAV URL: http://localhost:8800/baikal/html/dav.php/principals/
- Infcloud: http://localhost:8800/infcloud/
- Timezone: Europe/Paris

# Autodiscovery

You can support easy mail-style (*username@hostname.domain*) setup by with
configuring service discovery features as described.
[here](http://sabre.io/dav/service-discovery/).
> The docker container  contains these redirects setup already.

# HTTPs / SSL / TLS

The current setup does not support https directly and suppose an existing reverse proxy (e.g. https://traefik.io)

# Related projects

There are some other projects related to this topic, but
I couldnt find one using latest Alpine with php7 and a small httpd
having a small footprint eventually running it with sqlite.

- https://gitlab.com/Plague_Doctor/docker-baikal/

  Did not support php7 when I created this project and uses nginx instead of lighttpd. Unfortunately I did not find it on my research beforehand.
  The image is [published on docker](https://hub.docker.com/r/plaguedr/baikal)
  hub, which may be important for others

- https://github.com/bambocher/docker-baikal

  Uses older baikal version with php5 and without infcloud

- https://github.com/maschel/docker-baikal

  Did not work for me (uses php7 with Version 0.4.6 of Baikal [which had problems
  creating calendars for me](https://github.com/bjuretko/docker-baikal-infcloud/issues/8)). Does not incorporate InfCloud

- https://github.com/ckulka/baikal-docker

  As the project before. Supports builds for different archs and nginx/apache.
  I planned to incorporate infcloud with the image, [which it doesn't](https://github.com/ckulka/baikal-docker/issues/13) and build an own image running the service with non-root privileges.
